# Übungen zu Git-Advanced

Der dritte Übungsteil behandelt etwas fortgeschrittenere Techniken
bei der Arbeit mit Git. 

Alle Übungen sind derart beschrieben,
dass Sie mit der Git-Bash durchgeführt werden sollen.
Versuchen Sie nach Abschluss aller Übungen
die selben Übungen in der von Ihnen verwendeten IDE
(SolutionCenter, VSCode) durchzuführen.

Nicht alle Aufgaben können in allen IDEs gelöst werden.

Verwenden Sie während der Übung auch den Befehl `git help <Befehl>`,
um Informationen zu Git-Befehlen zu erhalten.
Die Hilfe sollte sich unter Windows im Browser öffnen.
Wenn sich die Hilfe in der Git-Bash öffnet,
kann mit den Pfeiltasten navigiert und
mit der 'q'-Taste die Ansicht geschlossen werden.
Beispiel: `git help rebase`.



## Übung 1 - git log und gitk

Clonen Sie sich das Git-Repository von
https://gitlab.com/titan_cb/my-dev-project.

Machen Sie sich mit dem Git-Repository vertraut:

* Welchen Inhalt hat das Repository nach dem Clonen?
* Welche Branches gibt es?

Verwenden Sie hierzu die Befehle `git branch` und `git log`
respektive das Tool `gitk`.

Scheuen Sie nicht davor die Hilfe der Befehle zu lesen:

```
git help log
git help branch
git help gitk
```

Achten Sie darauf, dass ohne Parameter nicht alle Branches angezeigt werden.

Versuchen Sie auch folgendes

```
git log HEAD --<tab>
```

Sie bekommen bei korrekt konfiguriertem Bash-Autocomplete alle Parameter angezeigt.


## Übung 2 - git reset

Wechseln Sie zum Repository aus Übung 1 oder
clonen Sie dieses erneut.

Sie möchten die Änderungen des vorletzten Commits
des `fb-parse-params`-Branches auf den Master übernehmen.
Welches ist der passende Git-Befehl?

Probieren Sie, diesen Commit (`94cbdb5`) mittels Cherry-Pick
auf den `master`-Branch zu übernehmen.

Machen Sie den Cherry-Pick Rückgängig.
Welches ist der passende Git-Befehl?

Versuchen Sie, den Befehl `reset` um den Branch zurück zu setzen.

Schauen Sie sich das Reflog an.
Hier finden Sie auch den Rückgängig gemachten Cherry-Pick
des vorletzten Commits des Feature-Branch.
Machen Sie die reset-Operation rückgängig.

Versuchen Sie den Feature-Branch in den `master`-Branch
zu mergen.

Kann dieser Merge auch Rückgängig gemacht werden? Wie?

Lesen/überfliegen Sie https://git-scm.com/book/de/v2/Git-Tools-Reset-entzaubert (deutsch)
bzw. https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified (englisch).

Probieren Sie, was bei 

* `--soft`
* `--mixed`
* `--hard`

mit dem Repository passiert.


## Übung 3 - git alias

Zwei weit verbreitete Aliases in der Git-Welt sind
`git lol` und `git lola`.

Sie sehen in der Git-Konfiguration wie folgt aus:

[alias]
    lol = log --graph --decorate --pretty=oneline --abbrev-commit
    lola = log --graph --decorate --pretty=oneline --abbrev-commit --all --date=local 

Verwenden Sie den Befehl `git config` um diese Aliases zu konfigurieren.

Lesen Sie in der Hilfe nach,
was die Parameter des Befehls `git log` in `git lola` bedeuten.

Verwenden Sie `git lol` und `git lola`.

Es gibt weitere Aliases, die oft verwendet werden.
Recherchieren Sie welche dies sind.


### Lösungsvorschlag

```
git config --global alias.lol "log --graph --decorate --pretty=oneline --abbrev-commit"
git config --global alias.lola "log --graph --decorate --pretty=oneline --abbrev-commit --all"
```

Git Aliases unter https://www.geeksforgeeks.org/introduction-to-git-aliases/


## Übung 4 - git rebase -i

> Achtung: machen Sie diese Änderungen
> immer nur für nicht veröffentlichte Teile der History!

Wechseln Sie zum Repository aus Übung 1:
clonen Sie dieses erneut,
damit Sie in einem sauberen Zustand mit der Übung beginnen.

Erstellen Sie auf dem `master`-Branch einen neuen Commit.

Erstellen Sie auf dem `master`-Branch einen weiteren neuen Commit.

Holen Sie mittels Cherry-Pick die beiden Commits auch auf den Feature-Branch.
Die beiden Commits sollten jetzt sowohl auf dem `fb-parse-params`
als auch auf dem `master`-Branch verfügbar sein.

Führen Sie nun zwei interaktive Rebases durch:

* auf dem `master`-brach sollen der commit "some comments in main program" entfernt werden
  und die beiden neuen Commits sollen zusammengeführt werden.
* auf dem `fb-parse-params` sollen die neuen beiden Commits wieder entfernt werden
  und die beiden bestehenden Commits sollen vertauscht werden
  und "add param to run..." soll durch "add parameters to run..." ersetzt werden.


### Lösungsvorschlag


```
cd /c/git-repositories
git clone https://gitlab.com/titan_cb/my-dev-project my-dev-project-2
cd my-dev-project-2
git checkout master

vim newfile    # bearbeiten der Datei und speichern
git add newfile && git commit -m "introduce newfile"

vim newfile    # bearbeiten der Datei und speichern
git add newfile && git commit -m "edit newfile"

git checkout fb-<tab>
git cherry-pick -x <commit-id-1>
git cherry-pick -x <commit-id-1>

git checkout master
git rebase -i HEAD~2    # verwenden Sie sqash

git checkout fb-<tab>
git rebase -i HEAD~4
      
      r 94cbdb5 add parameter to run specific command
      pick 996cf64 add dir as parameter
      d 26fe920 introduce newfile
      d 64b5fba edit newfile

```


## Übung 5 - git add -i und partielles Stagen

Arbeiten Sie auf dem Repository von Übung 4 weiter.

Ändern Sie die Datei `dogit.sh`:

* Fügen Sie einen Kommentar `some lib import` vor Zeile 3 ein.
* Fügen Sie am Ende der Datei einen Kommentar `EOF` ein.

Versuchen Sie nur den ersten Kommentar zu stagen/committen.

Probieren Sie nun den zweiten Kommentar mit `git add -i` zu stagen/committen.
Sie finden eine Anleitung unter
https://coderwall.com/p/u4vjkw/git-add-interactive-or-how-to-make-amazing-commits

### Lösungsvorschlag

```
git add --patch dogit.sh
(1/2) Diesen Patch-Block der Staging-Area hinzufügen [y,n,q,a,d,j,J,g,/,e,?]? y
(2/2) Diesen Patch-Block der Staging-Area hinzufügen [y,n,q,a,d,K,g,/,e,?]? n
git status
git commit -m "add only one comment"

git add -i
git commit -m "add second comment"
```


## Übung 6 - Hooks

Sehen Sie sich das Verzeichnis `.git/hooks` an.

Lesen Sie die Gerrit-Dokumentation zum `commit-msg`-Hook an:
https://gerrit-review.googlesource.com/Documentation/cmd-hook-commit-msg.html

Probieren Sie den Hook im Repository der vorherigen Übung zu aktivieren.

Und fügen Sie dem letzten Commit mittels `git commit --amend` eine Change-Id hinzu.

### Lösungsvorschlag

```
curl -Lo .git/hooks/commit-msg http://git.bachmann.at/test/tools/hooks/commit-msg
chmod +x .git/hooks/commit-msg
less .git/hooks/commit-msg

git commit --amend
git log -1
```

## Übung 7 - Stash

Experimentieren Sie in einem bestehenden Repository,
zum Beispiel das aus den vorherigen Übungen,
was mit den Stash-Operationen alles möglich ist.

Rekapitulieren Sie,
was `git stash` machent.

Verwenden Sie folgende Befehle:

* `git stash push`
* `git stash list` 
* `git stash pop` bzw. `git stash apply`

Was ist der Unterschied zwischen `pop` und `apply`?

Checken Sie einen Stash als Branch aus,
um die Änderungen in den bestehenden Stand mergen zu können.

Wie kann ein stash gelöscht werden?



## Letzte Übung

Gehen Sie die Übungen
1 (log=History),
2 (reset),
4 (rebase interactive),
5 (patrielles Staging) und
7 (Stash) noch einmal durch und
versuchen Sie diese in der von Ihnen verwendeten Entwicklungsumgebung umzusetzen.

