# Übungen zu Git-Basics

Der zweite Übungsteil behandelt das Arbeiten Arbeiten
mit entfernt liegenden Git-Repositories. 

Alle Übungen sind derart beschrieben,
dass sie mit der Git-Bash durchgeführt werden sollen.
Versuchen Sie nach Abschluss aller Übungen
die selben Übungen in der von Ihnen verwendeten IDE
(SolutionCenter, VSCode) durchzuführen.

Verwenden Sie während der Übung auch den Befehl `git help <Befehl>`,
um Informationen zu Git-Befehlen zu erhalten.
Die Hilfe sollte sich unter Windows im Browser öffnen.
Wenn sich die Hilfe in der Git-Bash öffnet,
kann mit den Pfeiltasten navigiert und
mit der 'q'-Taste die Ansicht geschlossen werden.
Beispiel: `git help log`.

## Vorbereitung: Diff Tool konfigurieren

Wir werden im Verlauf dieser Übung ein Diff-Tool benötigen.
Downloaden und installieren Sie ein grafisches Diff-/Merge-Tool.

### Lösungsvorschlag

Download von KDiff3

* Windows: https://www.heise.de/download/product/kdiff3-17087/download
* Linux: verwenden Sie den Package-Manager (yum, apt, ...) Ihrer Linux-Distribution.

Die Pfade in folgenden Befehlen müssen unter Umständen angepasst werden:


```
git config --global merge.tool kdiff3
git config --global mergetool.kdiff3.path "C:/Program Files/KDiff3/kdiff3.exe"
git config --global mergetool.kdiff3.trustExitCode false

git config --global diff.guitool kdiff3
git config --global difftool.kdiff3.path "C:/Program Files/KDiff3/kdiff3.exe"
git config --global difftool.kdiff3.trustExitCode false
```


## Übung 1 - Erste Schritte mit remotes

Klonen Sie das Repository mit den Schulungsunterlagen
in den Ordner mit Ihren Git-Repositories.

Untersuchen Sie das beim Clone entstandene Remote.
Es besteht aus zwei Teilen.
Welches sind diese Teile?

> Beachten Sie, dass das Repository das erstellte Remote
> zweimal enthält.
> Remotes können für fetch (ist ein Teil von pull) und
> für push unterschiedlich konfiguriert sein.

Entfernen Sie nun das automatisch erstellte Remote `origin` und
prüfen Sie, ob noch ein Remote konfiguriert ist.

Legen Sie ein neues Remote mit dem namen `gitlab` und
der URL `http://gitlab.com/titan_cb/git-schulung.git` an.

> Hinweis: Sie hätten den selben Effekt erhalten,
> wenn sie das Remote mit `git remote rename origin gitlab` umbenannt hätten.

Legen Sie ein weiteres Remote mit dem namen `gerrit` und
der URL `http://git.bachmann.at/test/git-schulung.git` an.

Überprüfen Sie nochmal die Remotes.

> Sie könnten
> - wenn das Repository an beiden Remotes verfügbar wäre - 
> von beiden Remotes fetchen (auch pullen) und
> pushen

### Lösungsvorschlag

```
cd /c/git-repositories
git clone http://gitlab.com/titan_cb/git-schulung.git
cd git-schulung
git remote -v
git remote remove origin
git remote -v
git remote add gitlab http://gitlab.com/titan_cb/git-schulung.git
git remote -v
git remote add gerrit http://atfkgit01/test/git-schulung.git
git remote -v
```

## Übung 2 - Refs

Untersuchen Sie im Repository aus Übung 1
die automatisch erstellten Refs und Refspecs.

Legen Sie eine leere Datei `emptyfile` an und
commiten Sie diese.

Untersuchen Sie nun nochmals die Refs.
Sie werden feststellen,
der Branch hat einen neuen Commit bekommen und
die Ref `refs/heads/master` zeigt nun auf den neuen Commit zeigt.

Lassen Sie sich alle Branches anzeigen.

> Sie können mit dem Befehl
> `git push -u <remote> <branch>` den sogenannten Upstream-Branch setzen.
> So wird sich der automatisch erstellte Remote-Tracking-Branch
> auch in den Refs manifestieren.



### Lösungsvorschlag

```
ls -al .git/refs/heads
cat .git/refs/heads/master
# alternativ: git show-ref --head
touch emptyfile
git add emptyfile && git commit -m "add empty file"
git show-ref --head
git branch --all
```



### Übung 3 - lokale Branches und einfache Merges

In dieser Übung werden wir uns lokal mit Branches beschäftigen.
Wir werden mit lokal angelegten Branches arbeiten.
Alles, was mit lokalen Branches durchgeführt werden wird,
gilt für Remote-Tracking-Branches auch.

Erstellen Sie ein neues lokales Git-Repository und
lassen Sie sich die lokalen Branches anzeigen.

Sie sehen, dass es noch keinen Branch gibt,
weil es keinen Commit gibt.

Erstellen Sie folgende Dateien mit sinnvollem Inhalt:

* `.gitignore` - verwenden Sie gitignore.io
* `README.md` - schreiben Sie mindestens eine erste Überschrift in die Datei:

```Markdown
# Erste Überschrift

Dies ist ein Repository für Branch-Experimente
```

Commiten Sie beide Dateien.
Prüfen Sie, ob jetzt der master-Branch erstellt wurde.

Legen Sie eine Datei `m100-code.c` mit etwas Code an:

```c
#include <stdio.h>
int main() {
   // printf() displays the string inside quotation
   printf("Hello m100 :-)");
   return 0;
}
```

Commiten Sie diese Datei.

Prüfen Sie nun, ob in der History zwei Commits vorhanden sind.

Legen Sie einen lokalen Branch `fb-m100-fix-critical` vom initialen Commit an und
wechseln Sie auf diesen Branch.

Prüfen Sie nun in der History, ob der zweite Branch existiert und
wo der HEAD steht.

Wechseln Sie zurück zum `master`-Branch und
prüfen Sie den HEAD.

Versuchen Sie nun den `master`-Branch in den `fb-m100-fix-critical`-Branch zu mergen.

Sie werden einen Fast-Forward-Merge sehen.

Commiten Sie auf dem `master`-Branch die Datei `m100-hack.c` mit folgendem Inhalt:

```c
#include <stdio.h>
int main() {
   printf("m100-hack-123");
   return 0;
}
```

Commiten Sie auf dem `fb-m100-fix-critical`-Branch die Datei `m100-hack.c` mit folgendem Inhalt:

```c
#include <stdio.h>
int main() {
   printf("m100-hack-abc");
   return 0;
}
```

Sie haben nun einen Merge-Konflikt zwischen diesen beiden Branches hergestellt.

Erstellen Sie von diesem Repository in genau diesem Zustand eine Kopie.

Mergen Sie den `fb-m100-fix-critical`-Branch in den `master`-Branch.
Verwenden Sie hierfür entweder einen Editor um die Konflikt-Marker zu entfernen oder
verwenden Sie `git mergetool`.

> Achtung: ein Merge ist erst nach `git add` und `git commit` abgeschlossen.

### Lösungsvorschlag

```Bash
cd /c/git-repositories
git init playing-with-branches-1
cd playing-with-branches-1
git branch --all

vim README.md    # es kann hierfür jeder Editor verwendet werden
vim .gitignore   # es kann hierfür jeder Editor verwendet werden

git add .
git commit -m "Initial commit"

git branch --all

vim m100-code.c

git add m100-code.c && git commit -m "add m100-code"

git log --oneline --graph --decorate --all  # verwenden Sie ggf. gitk hierfür

git checkout -b fb-m100-fix-critical  <commit-id>

git log --oneline --graph --decorate --all

git checkout master

git log --oneline --graph --decorate --all

git checkout fb-m100-fix-critical

git merge master

git log --oneline --graph --decorate --all


git checkout master
vim m100-hack.c && git add m100-hack.c && git commit -m "add some hack"


git checkout fb-<tab>   # verwenden Sie hier zur Abwechslung die Tabulatortaste für Auto-Complete
vim m100-hack.c && git add m100-hack.c && git commit -m "add some hack"

git log --oneline --graph --decorate --all

cd /c/git-repositories
cp -a playing-with-branches-1 playing-with-branches-2

git checkout master     # um in den master-Branch zu mergen,
                        # muss dieser erst ausgecheckt sein.
git merge fb-m100-fix-critical

vim m100-hack.c         # auflösen des Konflikts
git mergetool           # als Alternative

git add m100-hack.c
git commit

```

### Übung 4 - lokale Branches und Rebase

Wechseln Sie zur Kopie des Repositories.

Verwenden Sie ab und an den Befehl `git status`
um den Zustand des Git-Repositories zu erfahren.
Mit diesem Befehl sehen Sie auch auf welchem Branch
Sie sich derzeit befinden.

> Alternativ können Sie den Merge aus Übung 3 mit `git reset`
> Rückgängig machen: `git reset --hard <commit-id-letzter-commit-master>`

Erstellen Sie als erstes auf dem `fb-m100-fix-critical`-Branch einen weiteren Commit
indem Sie die Datei `m100-hack.c` wie folgt ändern:

```c
#include <stdio.h>
int main() {
   printf("m100-hack-abc");
   if (1) {
       return 1;
   }
   return 0;
}
```

Versuchen Sie nun den `fb-m100-fix-critical`-Branch auf den `master`-Branch zu rebasen.

Nehmen Sie für diese letzte Übung das frei verfügbare e-Book Pro-Git zur Hilfe:
https://git-scm.com/book/en/v2/Git-Branching-Rebasing


### Lösungsvorschlag


```
cd /c/git-repositories playing-with-branches-2

git status

git checkout fb-m100-<tab>

vim m100-h<tab>

git add m100-hack.c && git commit -m "extend hack"

```

## Übung 5 - Einfachen Workflow durch spielen

Klonen Sie sich das Git-Repository von
https://git.bachmann.at/test/git-demo-2/

Sie finden im Ordner `lib-project`
die Datei `hotfix.c`.

Fügen Sie in Zeile 4 einen FIXME-Kommentar ein:

```
   printf("m100-hack-abc");
   // FIXME: Fehler gefunden von <Benutzerkürzel>
   int *p;
```

Committen Sie die Änderung und pushen
Sie die Änderung ins Repository.

Sollte der Push nicht funktionieren,
war vermutlich ein anderer Schulungsteilnehmer schneller.
Dann müssen Sie die Änderung pullen und ggf. mergen.


## Letzte Übung

Gehen Sie die Übungen noch einmal durch und
versuchen Sie diese in der von Ihnen verwendeten Entwicklungsumgebung umzusetzen.
