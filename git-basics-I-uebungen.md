# Übungen zu Git-Basics

Der erste Übungsteil behandelt die grundlegenden Arbeiten
in einem Git-Repository. 

Alle Übungen sind derart beschrieben,
dass sie mit der Git-Bash durchgeführt werden sollen.
Versuchen Sie nach Abschluss aller Übungen
die selben Übungen in der von Ihnen verwendeten IDE
(SolutionCenter, VSCode) durchzuführen.

Verwenden Sie während der Übung auch den Befehl `git help <Befehl>`,
um Informationen zu einem Git-Befehl zu erhalten.
Die Hilfe sollte sich unter Windows im Browser öffnen.
Wenn sich die Hilfe in der Git-Bash öffnet,
kann mit den Pfeiltasten navigiert und mit der 'q'-Taste die Ansicht geschlossen werden.
Beispiel: `git help log`.

## Übung 1 - Bash

Wechseln Sie in der Git-Bash in den Ordner,
in dem Sie Ihre Git-Repositories ablegen wollen.
Sollte dieser Ordner noch nicht bestehen,
legen Sie den Ordner an.
Dieser Ordner sollte  `c:\git-repositories` oder `c:\git-repos` sein.
In der Git-Bash unter Windows werden die Laufwerksbuchstaben
im Root-Verzeichnis eingebunden.
Das Laufwerk `C:` heißt in der Git-Bash `/c/`.

Wechseln Sie mit `cd` in andere Verzeichnisse und
verwenden Sie dazu sowohl absolute als auch relative Pfade.

> Versuchen Sie die Tab-Taste
> für die Autovervollständigung und `Strg + R` um in der History zu suchen.

### Lösungsvorschlag

```bash
mkdir /c/git-repos
cd /c/git-repos
cd ../Windows/Temp/
mkdir /c/git-repos/empty-folder
cd ../../git-repos/emtty-folder
```


## Übung 2 - Git Konfiguration überprüfen

Überprüfen Sie, ob Ihr Name und Ihre E-Mail-Adresse
in der Git-Konfiguration hinterlegt sind:

```bash
git config --list
git config --list --show-origin

```

Sollte dies nicht der Fall sein,
holen Sie das bitte nach und achten dabei auf die Groß- und Kleinschreibung:

```
$ git config --global user.name "Maximilian MUSTERMANN"
$ git config --global user.email "Maximilian.MUSTERMANNU@bachmann.info"

```


## Übung 3 - Erste Git-Befehle

Legen Sie ein Git-Repository mit dem Namen "basics-1-uebung-1" an und
wechseln Sie in das Repository.
Schauen Sie sich den Inhalt des leeren Repositories an.

Es ist zwar davon abzuraten, im Ordner `.git` Änderungen zu machen,
aber es finden sich dort - zumindest nach dem ersten Commit -
ein paar interessante Dinge zu entdecken.

### Lösungsvorschlag

```bash
cd /c/git-repos
git init basics-1-uebung-1
cd basics-1-uebung-1
ls -alh
```

> Der letzte Schritt kann auch mit einem graphischen Werkzeug
> wie dem Windows-Explorer durchgeführt werden.


## Übung 4 - Commiten

Legen Sie mit einem Editor (notepad++, gedit, vim, nano) eine Quellcode-Datei
im Git-Repository `basics-1-uebung-1` an.

```c
#include <stdio.h>
int main() {
   // printf() displays the string inside quotation
   printf("Hello, World!");
   return 0;
}
```

Fügen Sie diese Datei dem Index hinzu und
sehen Sie sich den Status der Datei mit dem Befehl `git status` an.
Ändern Sie die Datei noch einmal in einem Editor.
Sehen Sie sich den Status der Datei noch einmal mit dem Befehl `git status` an.

Sie sehen die Datei zweimal im Status. Einmal im Index und einmal geändert.

Achten Sie bei dieser Übung auf die Statusänderungen der Datei.

Verwenden sie zum Anzeigen der History den Befehl `git log`.

Probieren Sie auch die Schalter `--oneline --graph --all`.

### Lösungsvorschlag

```bash
cd /c/git-repos
vim hello-world.c # mit beliebigem Editor erstellen
git status
git add hello-world.c
vim hello-world.c # bearbeiten und speichern
git status
git commit -m "add code change"
git status
git add hello-world.c
git status
git commit -m "add second change"
git log
```

> Verwenden Sie auch `gitk` um die History anzusehen.

## Letzte Übung

Gehen Sie alle Übungen durch und versuchen Sie diese in der von Ihnen verwendeten Entwicklungsumgebung umzusetzen.