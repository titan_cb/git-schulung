# Übungen zu Git-Advanced

Der vierte Übungsteil behandelt die Arbeit mit Git und Gerrit.

Alle Übungen sind derart beschrieben,
dass Sie mit der Git-Bash durchgeführt werden sollen.
Versuchen Sie nach Abschluss aller Übungen
die selben Übungen in der von Ihnen verwendeten IDE
(SolutionCenter, VSCode) durchzuführen.

Nicht alle Aufgaben können in allen IDEs gelöst werden.

Verwenden Sie während der Übung auch den Befehl `git help <Befehl>`,
um Informationen zu Git-Befehlen zu erhalten.
Die Hilfe sollte sich unter Windows im Browser öffnen.
Wenn sich die Hilfe in der Git-Bash öffnet,
kann mit den Pfeiltasten navigiert und
mit der 'q'-Taste die Ansicht geschlossen werden.
Beispiel: `git help rebase`.

Achten Sie bei der Durchführung der Übungen darauf,
dass Sie den vollen Domain-Namen der Gerrit-Test-Instanz
`git.bachmann.at` 
angeben sollten.
Stellen Sie vor dem Beginn der Übungen sicher,
dass Sie sich am Gerrit-Web-UI anmelden können.

> Achtung: gelegentlich muss die Anmeldung zweimal direkt hintereinander durchgeführt werden.
> lassen Sie sich davon nicht verunsichern. Es ist ein bekannter Fehler.


## Übung 1 - Clonen mit Git-Hook

Klonen Sie das Git-Repository `m300-system`
von `http://git.bachmann.at/test/` auf Ihren Computer.

Stellen Sie sicher,
dass der commit-msg hook in ihrem Repository vorhanden ist.

Prüfen Sie auf dem Web-UI,
ob es einen noch nicht submitteten Change gibt.

Laden Sie einen der vorhandenen Changes in Ihr lokales Repository.
Erstellen Sie einen einen neuen Patch in diesem Change:
schreiben Sie ihren Benutzernamen in einen Kommentar
in der Datei `m300-runtime/lib/init.sh`.

Schauen Sie sich die Review-Funktionen im Web-UI an:

* Welche Informationen sehen Sie über den Change?
* Was ist der Parent eines Changes?
* Überlegen Sie, was der Unterschied zwischen Assignee, Reviewer und CC

Fügen Sie einen Review-Kommentar zur Datei `src/lib/init.sh` hinzu.
Vergessen Sie nicht diesen zu speichern.


### Lösungsvorschlag

```bash

git clone "http://git.bachmann.at/test/m300-system" && (cd "m300-system" && mkdir -p .git/hooks && curl -Lo `git rev-parse --git-dir`/hooks/commit-msg http://git.bachmann.at/test/tools/hooks/commit-msg; chmod +x `git rev-parse --git-dir`/hooks/commit-msg)

less my-dev-project/.git/hooks/commit-msg

git fetch "http://git.bachmann.at/test/a/m300-system" refs/changes/98/998/1 && git checkout -b change-998-1 FETCH_HEAD

vim src/lib/init.sh     # bearbeiten der Datei

git add src/lib/init.sh && git commit --amend --no-edit && git push origin HEAD:refs/for/master

```


# Übung 2 - abhängige Changes

Verwenden Sie in dieser Übung das Hilfsmittel lokaler Branches
damit die Übersicht nicht verloren geht.

Erstellen Sie im Repository aus Übung 1 basierend auf `origin/master`
einen neuen Change.

Erstellen Sie basierend auf diesem Change einen weiteren.

Fügen Sie dem ersten Change ein weiteres Patchset hinzu.

überlegen Sie, was ist zu tun?

Bringen Sie den abhängigen Change 2 auf den neuesten Stand von Change 1.
Verwenden Sie hierfür einen Rebase.

### Lösungsvorschlag

```bash
# klonen Sie unter Umständen neu

git checkout -b fb-change-1 origin/master
vim         # Bearbeiten
git add
git commit -m "change-1"
git push origin HEAD:refs/for/master

git checkout -b fb-change-2
vim         # Bearbeiten
git add
git commit -m "change-2 (abhängig)"
git push origin HEAD:refs/for/master

git checkout fb-change-1
vim         # Bearbeiten
git add
git commit --amend -m "change-1 zweiter patch"

git checkout ...
git rebase ... 
```

# Übung 3 - reparieren eines vergessenen `--amend`

Erstellen Sie im Repository aus Übung 1 basierend auf `origin/master`
einen neuen Change.

Fügen Sie diesem Change ein weiteres Patch-Set hinzu. Vergessen Sie dabei bewusst das `--amend`.

Reparieren Sie diesen Zustand.


## Letzte Übung

Gehen Sie die Übungen noch einmal durch und 
versuchen Sie diese in der von Ihnen verwendeten Entwicklungsumgebung umzusetzen.

